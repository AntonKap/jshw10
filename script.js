let tabsNamesArry = document.querySelectorAll(".tabs-title");
let tabsTextArray = document.querySelectorAll(".tabs-text");

tabsNamesArry.forEach((tabsName, indexName) => {
    tabsName.addEventListener("click", ()=>{
        tabsTextArray.forEach((tabsText, indexText) => {
            if (indexName == indexText){
                tabsText.classList.toggle('text-unblock')
            }else{
                tabsText.classList.remove('text-unblock')
            }
        })
    })
})
